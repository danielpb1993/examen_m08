package com.example.examendanielpenarroya;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class MyAdapter2 extends RecyclerView.Adapter<MyAdapter2.MyViewHolder> {
    private ArrayList<Song> mSongs;
    private Context mContext;

    public MyAdapter2(ArrayList<Song> mSongs, Context mContext) {
        this.mSongs = mSongs;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);
        View view = layoutInflater.inflate(R.layout.song_row, parent, false);
        return new MyAdapter2.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Picasso.get().load(mSongs.get(position).getImageSong())
                .fit()
                .centerCrop()
                .into(holder.imageViewCD);
        holder.textNomCD.setText(mSongs.get(position).getNameSong());
        holder.textBandCD.setText(mSongs.get(position).getBandSong());

        holder.imageViewCD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, SongActivity.class);
                intent.putExtra("nameSong", mSongs.get(holder.getAdapterPosition()).getNameSong());
                intent.putExtra("bandSong", mSongs.get(holder.getAdapterPosition()).getBandSong());
                intent.putExtra("imageSong", mSongs.get(holder.getAdapterPosition()).getImageSong());
                intent.putExtra("yearSong", String.valueOf(mSongs.get(holder.getAdapterPosition()).getYearSong()));
                intent.putExtra("lyricsSong", mSongs.get(holder.getAdapterPosition()).getLyrics());
                mContext.startActivity(intent);
            }
        });

        holder.imagePlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.imagePlay.animate().rotation(360f).setDuration(1000);
                new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        holder.imagePlay.setVisibility(View.GONE);
                        holder.imagePlay2.setVisibility(View.VISIBLE);
                    }
                }, 1000);
            }
        });

        holder.imagePlay2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.imagePlay2.animate().rotation(360f).setDuration(1000);
                new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        holder.imagePlay.setVisibility(View.VISIBLE);
                        holder.imagePlay2.setVisibility(View.GONE);
                    }
                }, 1000);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mSongs.size();
    }

    public class MyViewHolder extends  RecyclerView.ViewHolder{
        private ImageView imageViewCD;
        private TextView textNomCD;
        private TextView textBandCD;
        private ImageView imagePlay;
        private ImageView imagePlay2;

        ConstraintLayout rowLayoutSong;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            imageViewCD = itemView.findViewById(R.id.imageViewCDSong);
            textNomCD = itemView.findViewById(R.id.textTitleSong);
            textBandCD = itemView.findViewById(R.id.textBandNameSong);
            imagePlay = itemView.findViewById(R.id.imagePlay1);
            imagePlay2 = itemView.findViewById(R.id.imagePlay2);
            rowLayoutSong = itemView.findViewById(R.id.rowLayoutSong);


        }

    }
}
