package com.example.examendanielpenarroya;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class SongActivity extends AppCompatActivity {
    private ImageView imageView;
    private TextView textTitleSong;
    private TextView textBandSong;
    private TextView textYearSong;
    private TextView textLyrics;

    String urlImage, nomSong, bandSong, yearSong, lyricsSong;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_song);
        imageView = findViewById(R.id.imageViewSongActivity);
        textTitleSong = findViewById(R.id.textNomSongActivity);
        textBandSong = findViewById(R.id.textBandSongActivity);
        textYearSong = findViewById(R.id.textYearSongActivity);
        textLyrics = findViewById(R.id.textLyricsSongActivity);

        getData();
        setData();
    }

    private void getData(){
        urlImage = getIntent().getStringExtra("imageSong");
        nomSong = getIntent().getStringExtra("nameSong");
        bandSong = getIntent().getStringExtra("bandSong");
        yearSong = getIntent().getStringExtra("yearSong");
        lyricsSong = getIntent().getStringExtra("lyricsSong");
    }

    private void setData(){
        textTitleSong.setText(nomSong);
        textBandSong.setText(bandSong);
        textYearSong.setText(yearSong);
        textLyrics.setText(lyricsSong);
        Picasso.get().load(urlImage)
                .fit()
                .centerCrop()
                .into(imageView);
    }
}