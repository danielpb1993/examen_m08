package com.example.examendanielpenarroya;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {
    private ArrayList<CD> mCds;
    private Context mContext;

    public MyAdapter(ArrayList<CD> mCds, Context mContext) {
        this.mCds = mCds;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);
        View view = layoutInflater.inflate(R.layout.cd_row, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Picasso.get().load(mCds.get(position).getImageCD())
                .fit()
                .centerCrop()
                .into(holder.imageViewCD);

        holder.textNomCD.setText(mCds.get(position).getNameCD());

        holder.rowLayoutCD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, CdActivity.class);
                intent.putExtra("nameCD", mCds.get(holder.getAdapterPosition()).getNameCD());
                intent.putExtra("bandCD", mCds.get(holder.getAdapterPosition()).getBandCD());
                intent.putExtra("infoCD", mCds.get(holder.getAdapterPosition()).getInfoCD());
                intent.putExtra("imageCD", mCds.get(holder.getAdapterPosition()).getImageCD());
                mContext.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return mCds.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        private ImageView imageViewCD;
        private TextView textNomCD;

        ConstraintLayout rowLayoutCD;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            imageViewCD = itemView.findViewById(R.id.imageViewCD);
            textNomCD = itemView.findViewById(R.id.textNomCD);
            rowLayoutCD = itemView.findViewById(R.id.rowLayoutCD);
        }
    }
}
