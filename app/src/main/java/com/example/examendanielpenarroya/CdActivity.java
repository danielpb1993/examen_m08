package com.example.examendanielpenarroya;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class CdActivity extends AppCompatActivity {
    private List<Song> songs;
    private ImageView imageViewCD;
    private TextView textNomCD;
    private TextView textBandCD;
    private TextView textInfoCD;


//    MyAdapter2 myAdapter2 = new MyAdapter2()
    String urlImage, nomCD, bandCD, infoCD;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cd);

        imageViewCD = findViewById(R.id.imageViewCDActivity);
        textNomCD = findViewById(R.id.textNomCDActivity);
        textBandCD = findViewById(R.id.textBandCDActivity);
        textInfoCD = findViewById(R.id.textInfoCDActivity);

        getData();
        setData();


    }

    private void getData(){
        urlImage = getIntent().getStringExtra("imageCD");
        nomCD = getIntent().getStringExtra("nameCD");
        bandCD = getIntent().getStringExtra("bandCD");
        infoCD = getIntent().getStringExtra("infoCD");

    }

    private void setData(){
        textNomCD.setText(nomCD);
        textBandCD.setText(bandCD);
        textInfoCD.setText(infoCD);
        Picasso.get().load(urlImage)
                .fit()
                .centerCrop()
                .into(imageViewCD);
    }
}